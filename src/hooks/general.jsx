import placeholder from '../assets/default-avatar-placeholder.png';
import professional from '../assets/icons/professional.svg';
import citizen from '../assets/icons/citizen.svg';

export function renderImage(image) {
    if (!image) {
        return placeholder;
    } else {
        return image;
    }
}

export function getDriverTypeIcon(driverType) {
    if (driverType === 'professional') {
        return professional;
    } else {
        return citizen;
    }
}