import React from 'react';
import Loader from '../../components/Loader/loader';
import Header from '../../components/Header/header';
import Drivers from '../Drivers/drivers';

export default class Main extends React.Component {
    constructor() {
        super();
        this.handleFilter = this.handleFilter.bind(this);
        this.state = {
            drivers: [],
            driversInitial: [],
            isLoading: true,
            isMobile: true
        }
    }

    componentDidMount() {
        // Fetch drivers from mock API - added timeout to make loader look better
        setTimeout(() => {
            fetch('http://private-05627-frontendnewhire.apiary-mock.com/contact_list')
                .then(res => res.json())
                .then((data) => {
                    this.setState({ drivers: data, driversInitial: data, isLoading: false });
                })
                .catch((err) => {
                    this.setState({ isLoading: false });
                    console.error(err);
                });
        }, 500);

        // Handle window resize
        window.addEventListener("resize", this.resize.bind(this));
        this.resize();
    }

    resize() {
        let currentMobileState = (window.innerWidth <= 576);
        if (currentMobileState !== this.state.isMobile) {
            this.setState({ isMobile: currentMobileState });
        }
    }

    render() {
        return (
            <div className={this.state.isMobile ? 'mobile' : 'desktop'}>
                <Header filterDrivers={this.handleFilter} />
                {
                    this.state.isLoading ?
                        (<Loader />) :
                        (<Drivers drivers={this.state.drivers} />)
                }
            </div>);
    }

    // Filter drivers list by search bar
    handleFilter(event) {
        let updatedList = this.state.driversInitial;
        updatedList = updatedList.filter(function (item) {
            return item.name.toLowerCase().search(
                event.target.value.toLowerCase()) !== -1;
        });
        this.setState({ drivers: updatedList });
    }
}