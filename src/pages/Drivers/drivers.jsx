import React from 'react';
import Driver from '../../components/Driver/driver';
import noData from '../../assets/icons/noData.png';
import './drivers.scss';

export default class Drivers extends React.Component {
    render() {
        if (this.props.drivers.length) {
            return (
                <div className="row container drivers-container">
                    {this.props.drivers.map((driver, index) => (
                        <Driver driver={driver} key={index} />
                    ))}
                </div>
            );
        } else {
            return (
                <div className="row container no-data">
                    <div className="col-12 text-center">
                        <img src={noData} alt="Not found" />
                        <h4 className="no-data-text">
                            Sorry, No drivers found.
                        </h4>
                    </div>
                </div>
            );
        }
    }
}