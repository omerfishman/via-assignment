import React from 'react';
import './App.scss';
import Main from './pages/Main/main';

export default function App() {
  return (
    <div>
      <Main />
    </div>
  );
}
