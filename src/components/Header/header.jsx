import React from 'react';
import './header.scss';
import search from '../../assets/icons/search.svg';

export default class Header extends React.Component {
    render() {
        return (
            <div className="header-container d-flex flex-wrap justify-content-between align-items-center">
                <div className="site-name">Contact List</div>
                <div className="search-container col-sm-4">
                    <input className="search-bar" type="search" placeholder="search..." onChange={this.props.filterDrivers} />
                    <img className="search-icon" src={search} alt="search" />
                </div>
            </div>
        );
    }
}