import React from 'react';
import './driver.scss';
import { getDriverTypeIcon, renderImage } from '../../hooks/general';

export default class Driver extends React.Component {
    render() {
        let driver = this.props.driver;
        let driverType = driver.driverType.toLowerCase();

        return (
            <div className="col-12 col-sm-4 col-lg-3 px-2 driver-container">
                <div className="card">
                    <img className="card-img-top" src={renderImage(driver.profile_image)} alt="profile" />
                    <div className="card-body position-relative bg-white">
                        <img className={"driver-type " + driverType} src={getDriverTypeIcon(driverType)} alt={driver.driverType} />
                        <div className="driver-name text-truncate h4">{driver.name}</div>
                        <div className="driver-rank">{driver.driverRank}</div>
                        <div className="driver-phone show-on-hover">
                            {
                                driver.phone ? <a href={"tel:" + driver.phone}>{driver.phone}</a> : '-'
                            }
                        </div>
                        <div className="driver-email show-on-hover text-truncate">
                            {
                                driver.email ? <a href={"mailto:" + driver.email}>{driver.email}</a> : '-'
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}