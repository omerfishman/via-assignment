import React from 'react';
import './loader.scss';
import Lottie from 'react-lottie';
import * as animationData from '../../assets/animations/car-loader.json';

export default class Loader extends React.Component {

    render() {
        const lottieOptions = {
            loop: true,
            autoplay: true,
            animationData: animationData.default,
        };

        return (
            <div className="row container data-loading">
                <div className="col-12 text-center">
                    <Lottie options={lottieOptions}
                        height={300}
                        width={300} />
                    <h4 className="loading-text">Loading..</h4>
                </div>
            </div>
        );
    }
}